import pickle
from email_types import Email, EmailAddress, EmailList

class PickledMailMessageEmailGetter():
    def __init__(self, pickle_file):
        self._pickle_file = pickle_file

    def get(self):
        with open(self._pickle_file, 'rb') as f:
            mail_messages = pickle.load(f)
        return self._convert_mail_messages_to_emails(mail_messages)

    def _convert_mail_messages_to_emails(self, mail_messages):
        emails = []
        for m in mail_messages:
            emails.append(self._email_from_mail_message(m))
        return emails

    def _email_from_mail_message(self, m):
        e = Email()
        e.from_ = EmailAddress(address=m.from_)
        e.subject = m.subject
        return e
