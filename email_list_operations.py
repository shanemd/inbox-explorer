
def count_emails_by_sender(emails):
    by_sender = _group_by_sender(emails)
    counts_by_sender = _get_counts_by_group(by_sender)
    sorted_by_count = _sort_by_item(counts_by_sender, 1)

    return sorted_by_count

def _group_by_sender(emails):
    grouped = {}
    for m in emails:
        key = m.from_.address
        if key not in grouped:
            grouped[key] = []
        grouped[key].append(m)
    return grouped

def _get_counts_by_group(groups):
    return {group : len(items) for (group, items) in groups.items()}

def _sort_by_item(groups, item):
    return sorted(groups.items(), key=lambda x: x[item], reverse=True)
