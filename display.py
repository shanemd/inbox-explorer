import tkinter as ttk
from page_models import CountsPageModel, EmailListPageModel


class TkDisplayManager():
    def __init__(self):
        self._root = ttk.Tk()

    def display_counts_page(self, model: CountsPageModel):
        tw = TableWindow(self._root, (model.counts_by, 'count'), model.rows)
        tw.render()

        self._root.mainloop()

    def display_email_list_page(self, model: EmailListPageModel):

        rows = [(e.from_.address, 'dummy subject') for e in model.emails]
        tw = TableWindow(self._root, ('Sender', 'Subject'), rows)
        tw.render()

        self._root.mainloop()


class TableWindow():
    def __init__(self, tk_root, titles, records):
        self._root = tk_root
        self._titles = titles
        self._records = records
        self._canvas = None
        self._scrollbar = None

    def render(self):
            self._canvas = ttk.Canvas(self._root, borderwidth=10)
            self._scrollbar = ttk.Scrollbar(self._root, orient='vertical', command=self._canvas.yview)
            self._canvas.configure(yscrollcommand=self._scrollbar.set)

            frame = ttk.Frame(self._canvas)


            self._scrollbar.pack(side="right", fill="y")
            self._canvas.pack(side="left", fill="both", expand=True)
            self._canvas.create_window((4,4), window=frame, anchor="nw")

            frame.bind("<Configure>", lambda event, canvas=self._canvas: self._onFrameConfigure(self._canvas))

            self._canvas.bind_all("<Button-4>", lambda event, canvas=self._canvas: self._onDownMouseWheel(canvas, event))
            self._canvas.bind_all("<Button-5>", lambda event, canvas=self._canvas: self._onUpMouseWheel(canvas, event))


            for i in range(0, len(self._titles)):
                ttk.Label(frame, text=self._titles[i]).grid(column=i, row=0)

            for i in range(0, len(self._records)):
                for j in range(len(self._records[i])):
                    ttk.Label(frame, text=self._records[i][j]).grid(column=j, row=i+1)

    def _onFrameConfigure(self, canvas):
        '''Reset the scroll region to encompass the inner frame'''
        canvas.configure(scrollregion=canvas.bbox("all"))

    def _onUpMouseWheel(self, canvas, event):
        canvas.yview_scroll(1, "units")

    def _onDownMouseWheel(self, canvas, event):
        canvas.yview_scroll(-1, "units")


    def clear(self):
        self._canvas.pack_forget()
        self._scrollbar.pack_forget()
