from dataclasses import dataclass
from email_types import EmailList
from typing import Tuple, List


@dataclass
class CountsPageModel():
    counts_by: str
    rows: List[Tuple[str, int]]

@dataclass
class EmailListPageModel():
    title: str
    emails: List[EmailList]
