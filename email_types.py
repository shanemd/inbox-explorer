from dataclasses import dataclass, field
from typing import List

@dataclass
class EmailAddress():
    name: str = ''
    address: str = ''

@dataclass
class Email():
    from_ : EmailAddress = None
    to: List[EmailAddress] = field(default_factory=list)
    cc: List[EmailAddress] = field(default_factory=list)
    bcc: List[EmailAddress] = field(default_factory=list)

    subject: str = ''
    body: str = ''

class EmailList():
    pass
