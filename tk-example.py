import tkinter

window = tkinter.Tk()
frame = tkinter.Frame(window)
frame.grid()
tkinter.Label(frame, text="Hello World!").grid(column=0, row=0)
tkinter.Button(frame, text="Quit", command=window.destroy).grid(column=0, row=1)

window.mainloop()
