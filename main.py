import sys

from email_getters import PickledMailMessageEmailGetter
from page_models import CountsPageModel, EmailListPageModel
from display import TkDisplayManager
from email_list_operations import count_emails_by_sender


def main(argv):
    if (len(argv) != 2):
        print('usage: python3 inbox-insights.py <path_to_mail_file.pkl>')
        return 1

    emails = PickledMailMessageEmailGetter(argv[1]).get()

    #page = CountsPageModel(counts_by='Sender', rows=count_emails_by_sender(emails))
    #TkDisplayManager().display_counts_page(page)

    page = EmailListPageModel(title="All Emails", emails=emails[:200])
    TkDisplayManager().display_email_list_page(page)

if __name__ == '__main__':
    main(sys.argv)
