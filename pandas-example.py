import pandas as pd

df = pd.DataFrame(
    {
        "from" : ['a@b.c', 'd@e.f', 'g@h.i'],
        "subject" : ['ABC', 'DEF', 'GHI'],
    })

print(df)
print()

grouped = df.groupby('from')

for name, group in grouped:
    print(name)
    print(group)
    print()

    def emails_as_dataframe(emails):
    return pd.DataFrame({
        'date' : [m.date.strftime('%Y-%m-%d') for m in emails],
        'from' : [m.from_ for m in emails],
        'to'   : [m.to for m in emails],
        'subject' : [m.subject for m in emails],
    })
