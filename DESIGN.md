
First panel - sorted by 'total emails':
sender  |  total emails  |  average days between emails


Concerns:
0. Defining what an 'email' is.            - email_types.py
1. Getting emails from some source         - email_getters.py
2. Grouping and sorting emails             - ?
  - Defining custom groupings              - ?
3. Displaying lists and groups of emails   - ?
4. Switching between displays              - ?
